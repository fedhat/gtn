
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = fista_EG(xgg0, f, g, par) 

% fista version inn the space EG with the regularizer  norm_EG(Vgg, '1;si,1')

if nargin == 0
    clc
    close all
    
    addpath('../utilities')
    
    par.DEBUG = true
    
    %temporary
    G={[1 3] [1 3 4];...
        [1 2 3] [2 3]};
    W = rand(5);
    
    projOnSmallSpace(W, G, 1);
    
    isDisjoint(G, [5 5])
    
    Wgg = zeros_EG(G)
    Vgg= ones_EG(G)
    Zgg = eye_EG(G)
    
    norm_EG(Vgg, 'fro')
    norm_EG(Vgg, '1;si,1')
    
    scalProd_EG(Vgg,Zgg)
    
    s2 = plus_EG(Vgg, Zgg)
    s3 = times_EG(Vgg, Zgg)
    s4 = times_scal_EG(5,Zgg)
    is_EG(Zgg, G)
    
    d =6; k=5
    W=rand(d,k)
    W2 = assemble_EG(op_adj_EG(W, G), G, [d k])
    
    W-W2
      
    
    
    % -----------------------
    clear all
    
    addpath('../utilities')
    d = 100;
    k = 110;
    r = floor(d*0.28);
    c = floor(k*0.62);
    
    G = {...
        (1:r)'     (1:c)'  ;...
        ((r+1):d)' (1:c)'  ;...
        (1:r)' ((c+1):k)'  ;...
        (35:50)'  (1:k)' ; ...  % new
        ((r+1):d)' ((c+1):k)' ...
        };
    
    
    %     G = {...
    %         %     (1:d)' (1:k)' ;...
    %         (1:r)'     (1:c)'      ;...
    %         ((r+1):d)' (1:c)'      ;...
    %         (1:r)'     (1:k)'      ;...
    %         ((r+1):d)' (1:k)'      ;...
    %         (1:r)'     ((c+1):k)'  ;...
    %         ((r+1):d)' ((c+1):k)'  ...
    %         };
    
    par.dims = [d,k];
    
    par.lambda = 20;
    
    par.itMax = 100;
    
    A = rand(d,k);
    Aa = @(Wgg) assemble_EG(Wgg, G, [d k]);
    f = @(Xgg) 0.5 * norm(Aa(Xgg)-A, 'fro')^2;
    
    g = @(Xgg) op_adj_EG(Aa(Xgg)-A, G);
    xgg0 = zeros_EG(G); 
    
    par.tol = 0.0001 ;
    
    par.VERSION = 'backtracking';
      
    
    disj =  isDisjoint(G, [d k])
    par.G = G;
    
    out = fista_EG(xgg0, f, g, par);
    hold on
    plot(out.times, out.erisk, 'rx-')
    title('FISTA')
    xlabel('time')
    set(gca,'yscale','log')
    
    
    figure
    imagesc(Aa(out.sol.W))
    colormap(1-gray)
    colorbar
    title('solution W*')
    'v'
    
else
    VERSION = getFromStruct(par,'VERSION','backtracking');
    
    switch VERSION
        
        case 'backtracking'
            % version with backtracking , all variables here are in EG
            %  with stop criterium if erisk is non-decreasing
            %==================
            %% parse input
            %==================
            
            G = par.G;
            nG = size(G,1);
            
            X = xgg0; % in EG
            
            tol = getFromStruct(par, 'tol', 0.05);
            itMax = getFromStruct(par,'itMax', 10);
          %  DISJ = getFromStruct(par,'isDisjoint', false);
            alpha = getFromStruct(par, 'alpha', ones(1, nG));
            DEBUG = getFromStruct(par,'DEBUG', false);
            ERISK_MAX = 10^10; %if the empirical risk gets bigegr, we stop
            
            % Lipschitz constant of gradient of f
            L = getFromStruct(par, 'lip_0', 0.01); %L_bar
            eta = 2; % eta>1
            
            la = par.lambda;
            
            Y_plus = X; %y_1
            t_plus = 1;
            
            zer = zeros(1, itMax);
            ts = zer;
            iters = zer;
            
            
            it = 1;
            
            %controls
            CONTINUE = true;
            LAST_ERISK = Inf;
            
            % metrics
            erisk = zer;
            reg =  -1* ones(1, itMax); %regularizer
            erisk_0 = f(X);
            
            reg_0 = norm_EG(X,'1;si,1', alpha);
            
            %================
            %%% loop
            %================
            while CONTINUE
                fprintf(':')
                time0 = cputime();
                
                % t_plus == t_{k+1}
                % y_plus == y_{k+1}
                % x_minus == x_{k-1}
                
                t = t_plus;
                X_minus = X;
                Y = Y_plus;
                
                %%% benchmark step linesearch %%%
                %%%%% this line is for p=1  %%%%
                if DEBUG
                    L =  0.01; %  restart always
                end
                
                CHECK_BACKTRACKING = true;
                while CHECK_BACKTRACKING
                    
                    mY =  minus_EG(Y, times_scal_EG(1/L, g(Y))); % = Y-1/L*g(Y)
                    X = groupNuclearNorm_prox_EG(mY, la/L, G, alpha);
                    fX = f(X);
                    
                    XmY = minus_EG(X,Y);
                    if fX <= f(Y) + scalProd_EG(XmY, g(Y)) + L/2 * norm_EG( XmY, 'fro')^2;
                        CHECK_BACKTRACKING = false;
                    else
                        L = eta * L;
                        fprintf('.')
                    end
                end
                
                t_plus = 0.5*(1 + sqrt(1 + 4*t^2));
                Y_plus = plus_EG(X, times_scal_EG(((t-1)/t_plus) , minus_EG(X,X_minus)));
                
                
                %% metrics
                ts(it) = cputime()-time0;
                iters(it) = it;
                erisk(it) = fX;
                reg(it) = norm_EG(X,'1;si,1', alpha); % groupNuclearNorm_forDisjoint(X, G, alpha);
                
                % check stop criterium
                if abs(erisk(it)-LAST_ERISK)< tol || it >= itMax || erisk(it)> ERISK_MAX
                    CONTINUE = false;
                else
                    it = it+1;
                    LAST_ERISK = erisk(it);
                end
                
            end
            
            %% metrics at last iteration
            metrics.obj = erisk(it) + la * reg(it);
            metrics.regEucl = norm_EG(X,'fro');
            metrics.reg = reg(it);
            metrics.erisk = erisk(it);
            metrics.iters = iters(it);
            
            
            
            %% outputs
            times = cumsum(ts);
            
            out = struct('iters', iters(1:it), 'times', times(1:it),...
                'erisk_0', erisk_0, 'reg_0', reg_0, 'erisk', erisk(1:it),...
                'lambda', la, 'reg', reg(1:it), 'it_tot', it, 'metrics', metrics);
            out.G = G;
            out.sol.W = X;
            
            
            
        otherwise
            error('misspelling')
    end
    
end
end



function OUT_G = groupNuclearNorm_prox_EG(Wgg, k, G, alpha) 
% this is the prox working on the product space EG

% Wgg: the variable in the space EG
% G: indicates the groups
% k: the constant of the prox
% alpha: facultative weights, vector of length number of groups

% G contains indeces: G={row col; row col; row col}
%                       {    g1 ;    g2  ;   g3   }
% where each pair "row col" is relates to the same group

% OUT: a sequence of (W_g)_g in E_G

nG = size(G,1);

if nargin < 4
    warning('alpha not defined')
    alpha = ones(1, nG);
end

P = cell(1,nG); % will contain the prox

for g = 1:nG
    Wg = Wgg{g}; % current block in RDY
    P_Wg = nuclearNorm_prox(Wg, k * alpha(g)) ;
    P{g} = P_Wg;
end

OUT_G = P;

end

