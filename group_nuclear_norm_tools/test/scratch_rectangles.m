%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2016         %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function scratch_rectangles()

close all

x = randn(500,500);
imagesc(x);
colorbar()
colormap(gray)

%hold on
rectangle('Position',[1 1 200 200],...
    'LineWidth', 3,   ...
    'EdgeColor','m')


rectangle('Position',[10 40 200 100],...
    'LineWidth', 3,   ...
    'EdgeColor','c')

%=================================================

close gcf

% matrix size
d = 500;
k = 500;

% 4 disjoint groups
rr = floor(0.65 * d);
cc = floor(0.75 * k);

G = {...
    (1:rr)'     (1:cc)'  ;...
    ((rr+1):d)' (1:cc)'  ;...
    (1:rr)' ((cc+1):k)'  ;...
    ((rr+1):d)' ((cc+1):k)' ...
    };


X = zeros(d,k);


c = {'c' 'm' 'y'}
nG = size(G,1);


for g = 1:nG
    
    
    yt =  min(G{g,1});
    yb =  max(G{g,1});
    xl =  min(G{g,2});
    xr =  max(G{g,2});
    
    h = yb-yt;
    w = xr-xl;
    
    rectangle('Position',[xl yt w h] ,...
        'LineWidth',2,   ...
        'EdgeColor', c{randi(3)}  )
    
    
end


close gcf

x = randn(500,500);
imagesc(x);
colorbar()
colormap(gray)


draw_groups(G,[])

end
