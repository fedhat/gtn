%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function xSoft = soft_thresholding(X, eps)
%X: real, vector or matrix
%eps: real>0


% xSoft= x-eps if x > eps;
%        0     if x in [-eps,eps]
%        x+eps if x < -eps;

if nargin == 0
    close all
    
    
    r = 2
    x = -5 : 0.1 : 5
    
    y= soft_thresholding(x, r);
    plot(x, y, 'xr')
    
    
    X=randn(40, 40)
    Y= soft_thresholding(X, r)
    figure
    plot(X(:),Y(:),'xg')
    
else
    
    assert(eps >= 0)
    xSoft=max(0, X - eps) + min(0, X + eps);
    
end