%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = fista_base(x0, f, g, par)   
% min F(W)= f(W) + lambda * ||W||

% where the norm is Group Schatten norm,
% g is the gradient of f
% f is L-smooth (argument not needed to run algo with constant step,
%                needed for backtracking)
% par.L Lipschitz constant of g


if nargin ==0
    close all
    
    addpath('../utilities');
    
    
    set(0, 'DefaultLineLineWidth', 3)
    set(0, 'DefaultAxesFontSize', 18)

    
    d = 100;
    k = 120;
    
    x0 = zeros(d, k);
    par.regularizer = 'nuclearNorm'; % GroupSchattenNorm
    %  par.p = 1;
    
    par.lip = 2; % Lipschitz constant of \nabla f
    par.lambda = 20;
    
 %   par.stepType = 'constant'; % 'constant' or 'backtracking'
    
    par.itMax = 30;
    par.tol = 0;
    
    
    par.computeAllOutputs = true;
    
    A = randn(d,k);
    f = @(X) 0.5 * norm(X - A,'fro')^2 ;
    g = @(X) X - A;
    
    par.VERSION = 'constStep'
    out = fista_base(x0, f, g, par);
    plot(out.times, out.erisk, 'ro-')
    title('FISTA')
    set(gca,'yscale','log')
   
    par.VERSION = 'backtracking'
    out_bk = fista_base(x0, f, g, par);
    hold on 
    plot(out_bk.times, out_bk.erisk, 'bo-')
    title('FISTA')
    set(gca,'yscale','log')
    
    legend({'const step' 'backtracking'})
    
    figure
    imagesc(out_bk.sol.W)
    colormap(gray)
    colorbar
    'v'
    
else
    
    
    
    VERSION = getFromStruct(par,'VERSION','backtracking');
    
    switch VERSION
        case 'backtracking'
            % version with backtracking % 25-11-2015
            %  with stop criterium if erisk non diminuisce
            %==================
            %% parse input
            %==================
            ALL_OUTPUTS = getFromStruct(par, 'computeAllOutputs', false);
            
            tol = getFromStruct(par, 'tol', 0.001);
            
            
            % define q
            %%% for p==1 %%%
            %q = Inf;
            
            X = x0; % x_0
            
            [d, k] = size(X);
            
            itMax = par.itMax;
            
            % Lipschitz constant of gradient of f
            L = getFromStruct(par, 'lip_0', 0.01); %L_bar
            eta = 2; % eta>1
            
            la = par.lambda;
            
            Y_plus = X; %y_1
            t_plus = 1;
            
            ts = zeros(1, itMax);
            iters = zeros(1, itMax);
            it = 1;
            
            %controls
            CONTINUE = true;
            LAST_ERISK = Inf;
            
            % metrics
            erisk = zeros(1, itMax);
            erisk(1) = f(X);
            
            if ALL_OUTPUTS
                reg = zeros(1, itMax);
                
                %%%% p==1 %%%%
                reg(1) = sum(svd(X));
                
                obj = erisk + la * reg;
            end
            
            
            
            
            %================
            %% loop
            %================
           % for it = 1:itMax
            while CONTINUE    
                fprintf(':')
                time0 = cputime();
                
                % t_plus == t_{k+1}
                % y_plus == y_{k+1}
                % x_minus == x_{k-1}
                
                t = t_plus;
                X_minus = X;
                Y = Y_plus;
                
                
                %%% benchmark step linesearch %%%
                %%%%% this line is for p=1  %%%%
                CHECK_BACKTRACKING = true;
                while CHECK_BACKTRACKING
                    
                    X = nuclearNorm_prox(Y - 1/L * g(Y), la/L); %d x k
                    fX = f(X);
                    if fX <= f(Y) + scalProd(X - Y, g(Y)) + L/2 * norm(X - Y, 'fro').^2 
                        CHECK_BACKTRACKING = false;
                    else
                        L = eta * L;
                    end
                end
                                
                t_plus = 0.5 * (1 + sqrt(1 + 4 * t^2));
                Y_plus = X + ((t - 1) / t_plus) * (X - X_minus);
                
                ts(it) = cputime() - time0;
                iters(it) = it;
                erisk(it) = fX;
             
                if ALL_OUTPUTS
                    %%% p==1 %%%
                    reg(it) = sum(svd(X));
                    obj(it) = erisk(it) + la * reg(it);
                    
                end
                
    %tmp              %abs((erisk(it)-LAST_ERISK)/(LAST_ERISK+1))
%tmp                  erisk(it)-LAST_ERISK;
                  
                % check stop criterium
                if abs(erisk(it) - LAST_ERISK) < tol || it >= itMax
                    CONTINUE = false
                else
                    it = it + 1;
                    LAST_ERISK = erisk(it);
                end
            end
            
            times = cumsum(ts);
            
            if ALL_OUTPUTS
                out = struct('iters', iters(1:it), 'times', times(1:it),'erisk', erisk(1:it),...
                    'reg', reg(1:it), 'obj', obj(1:it), 'lambda', la, 'it_tot', it);
            end
            
            out.sol.W = X;
  
        case 'backtracking_v1_1'
            % version with backtracking % 25-11-2015
            
            %==================
            %% parse input
            %==================
            ALL_OUTPUTS = getFromStruct(par, 'computeAllOutputs', false);
            
            
            
            % define q
            %%% for p==1 %%%
            %q = Inf;
            
            X = x0; % x_0
            
            [d, k] = size(X);
            
            itMax = par.itMax;
            
            % Lipschitz constant of gradient of f
            L = getFromStruct(par, 'lip_0', 0.01); %L_bar
            eta = 2; % eta>1
            
            la = par.lambda;
            
            Y_plus = X; %y_1
            t_plus = 1;
            
            ts = zeros(1, itMax);
            iters = zeros(1, itMax);
            
            
            if ALL_OUTPUTS
                erisk = zeros(1, itMax);
                reg = zeros(1, itMax);
                
                erisk(1) = f(X);
                %%%% p==1 %%%%
                reg(1) = sum(svd(X));
                
                obj = erisk + la * reg;
            end
            
            
            
            
            %================
            %% loop
            %================
            for it = 1:itMax
                
                fprintf(':')
                time0 = cputime();
                
                % t_plus == t_{k+1}
                % y_plus == y_{k+1}
                % x_minus == x_{k-1}
                
                t = t_plus;
                X_minus = X;
                Y = Y_plus;
                
                
                %%% benchmark step linesearch %%%
                %%%%% this line is for p=1  %%%%
                CHECK_BACKTRACKING = true;
                while CHECK_BACKTRACKING
                    
                    X = nuclearNorm_prox(Y - 1 / L * g(Y), la / L); %d x k
                    
                    if f(X) <= f(Y) + scalProd(X - Y, g(Y) ) + L/2 * norm(X - Y, 'fro').^2 
                        CHECK_BACKTRACKING = false;
                    else
                        L = eta * L;
                    end
                end
                
                X=X;
                
                
                t_plus = 0.5 * (1 + sqrt(1 + 4 * t^2));
                
                Y_plus = X + ((t - 1) / t_plus) * (X - X_minus);
                
                ts(it) = cputime() - time0;
                
                iters(it) = it;
                
                if ALL_OUTPUTS
                    erisk(it) = f(X);
                    %%% p==1 %%%
                    reg(it) = sum(svd(X));
                    obj(it) = erisk(it) + la * reg(it);
                    
                end
                
            end
            
            times = cumsum(ts);
            
            %error('under construction')
            
            if ALL_OUTPUTS
                out = struct('iters', iters, 'times', times,'erisk', erisk,...
                    'reg', reg, 'obj', obj, 'lambda', la);
            end
            
            out.sol.W = X;
            
        case 'constStep'
            % version with constant Lipschitz constant
            
            %==================
            %% parse input
            %==================
            ALL_OUTPUTS = getFromStruct(par, 'computeAllOutputs', false);
            
            
            
            % define q
            %%% for p==1 %%%
            %q = Inf;
            
            X = x0; % x_0
            
            [d, k] = size(X);
            
            itMax = par.itMax;
            
            % Lipschitz constant of gradient of f
            L = par.lip;
            la = par.lambda;
            
            Y_plus = X; %y_1
            t_plus = 1;
            
            ts = zeros(1, itMax);
            iters = zeros(1, itMax);
            
            
            if ALL_OUTPUTS
                erisk = zeros(1, itMax);
                reg = zeros(1, itMax);
                
                erisk(1) = f(X);
                %%%% p==1 %%%%
                reg(1) = sum(svd(X));
                
                obj = erisk + la * reg;
            end
            %================
            %% loop
            %================
            for it = 1:itMax
                
                fprintf(':')
                time0 = cputime();
                
                % t_plus == t_{k+1}
                % y_plus == y_{k+1}
                % x_minus == x_{k-1}
                
                t = t_plus;
                X_minus = X;
                Y = Y_plus;
                
                
                %%% fix step linesearch, depends on L %%%
                %%%%% this line is for p=1  %%%%
                X = nuclearNorm_prox(Y - 1/L * g(Y), la/L); %d x k
                
                
                t_plus = 0.5 * (1 + sqrt(1 + 4 * t^2));
                
                Y_plus = X + ((t -1) / t_plus) * (X - X_minus);
                
                ts(it) = cputime() - time0;
                
                iters(it) = it;
                if ALL_OUTPUTS
                    erisk(it) = f(X);
                    %%% p==1 %%%
                    reg(it) = sum(svd(X));
                    obj(it) = erisk(it) + la*reg(it);
                    
                end
                
            end
            
            times = cumsum(ts);
            
            %error('under construction')
            
            if ALL_OUTPUTS
                out = struct('iters', iters, 'times', times,'erisk', erisk,...
                    'reg', reg, 'obj', obj, 'lambda', la);
            end
            
            out.sol.W = X;
            
        case 'constStep_v1'
            
            
            p = par.p;
            assert(p >= 1);
            if p==1
                q = Inf;
            else if p== Inf
                    q = 1;
                else q = 1/(1- 1/p);
                end
            end
            
            X = x0; % x_0
            
            [d, k] = size(X);
            
            itMax = par.itMax;
            
            % Lipschitz constant of gradient of f
            L = par.lip;
            la = par.lambda;
            
            Y_plus = X; %y_1
            t_plus = 1;
            
            ts = 0;
            %================
            %% loops
            %================
            for it = 1:itMax
                
                fprintf(':')
                time0 = cputime();
                
                t = t_plus;
                %     clear t_plus
                
                X_minus = X;
                %    clear X
                
                Y = Y_plus;
                %   clear Y_plus
                
                % t_plus = t_{k+1}
                % y_plus = y_{k+1}
                % x_minus = x_{k-1}
                
                %        grad_cur = g(Y);
                
                
                switch p
                    case 1
                        % this line is for p=1
                        X = nuclearNorm_prox(Y-1/L* g(Y), la/L); %d x k
                    otherwise
                        error('non implementato')
                end
                
                t_plus = 0.5*(1 + sqrt(1 + 4*t^2));
                
                Y_plus = X + ((t-1)/t_plus) * (X - X_minus);
                
                ts(end+1) = cputime()-time0;
                
                hold on
                plot(it, f(X), 'ro')
                set(gca,'yscale','log')
                %error('under construction')
                
                
            end
            
            times = cumsum(ts);
            
            
    end
    
end
end



