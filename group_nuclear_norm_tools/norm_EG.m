%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = norm_EG(Wgg, TYPE, alpha)
% alpha : weights used only for non Frobenius norm

assert(iscell(Wgg));
nG =length(Wgg);


if nargin < 2
    type = 'fro';
end


switch TYPE
    case 'fro' %= '2;2' % euclidean norm of the euclidean norm
        a = 0;
        for g = 1:nG
            a = a + norm(Wgg{g},'fro')^2;
        end
        out = sqrt(a);
        
    case  '1;si,1' % norm 1 of nuclear norm
        
        if nargin < 3
            warning('alpha not defined')
            alpha = ones(1,nG);
        end
        
        a = 0;
        for g = 1:nG
            a = a + alpha(g) * sum(svd(full(Wgg{g})));
        end
        out = a;
        
    otherwise
        TYPE
        error('misspelling')
end

end

% MATLAB's documentations says:

%   Note svds is best used to find a few singular values of a large, sparse matrix. To find all the singular values of such a matrix, svd(full(A)) will usually perform better than svds(A,min(size(A)))