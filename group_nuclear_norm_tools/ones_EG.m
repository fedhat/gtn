%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function OUT_EG = ones_EG(G) 
% generates an element in EG with  matrices filled with ones

nG = size(G, 1);
OUT_EG = {};
for g = 1:nG
    OUT_EG{g} = ones(length(G{g, 1}), length(G{g, 2}));
end

end



