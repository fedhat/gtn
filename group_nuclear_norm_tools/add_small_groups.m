%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function G_out = add_small_groups(G, num_groups, d, k, width, height)
G_out = G;

for i = 1:num_groups
    assert(width <= k)
    assert(height <= d)
    y1 = 1 + floor(rand(1) * (d - height));
    y2 = y1 + height - 1;
    x1 = 1 + floor(rand(1) * (k - width));
    x2 = x1 + width - 1;
    
    G_out{end+1, 1} = (y1:y2)';
    % end+1 because it adds a new line
    G_out{end, 2} = (x1:x2)';
end
end