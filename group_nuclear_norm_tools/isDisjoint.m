%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = isDisjoint(G, dims)

%returns true when the property is validated
%    out.disjoint: 1
%  out.recoversSpace: 1

T = zeros(dims);
nG = size(G, 1);

for i=1:nG
    W_g = immersionInBigSpace(1, G, i, dims);
    T = T + W_g;
end

%W should contain here only zeros and ones
out.disjoint = isempty(find(T(:) >= 2));
out.coversSpace = isempty(find(T(:) == 0));

end
