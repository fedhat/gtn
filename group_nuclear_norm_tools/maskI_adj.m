%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function W = maskI_adj(x, I, r, c)
% I: vector of indices
% x: entries to merge in W
% W: matrix

%W = sparse(r, c);
%W(I)= x; %very slow for large scale

W = sparse(I, 1, x, r * c, 1);
W = reshape(W, r, c);

end