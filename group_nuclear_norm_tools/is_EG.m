%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function b = is_EG(Wgg, G)
% verifies that Wgg is in the space defined by groups G

% also the order of cells counts!
nG = size(G,1);

b = false;

% we put a try because if the structure if Wgg is something strange, 
% certainly it is not a type EG.
try
    for g = 1:nG
        c(g) = (size(Wgg{g},1) == length(G{g,1})) ...
            & (size(Wgg{g},2) == length(G{g,2}));
    end
    
    if all(c == 1)
        b = true;
    end
end


end



