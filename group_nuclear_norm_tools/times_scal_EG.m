%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function OUT = times_scal_EG(k, Wgg)  
% overloading of the product of Wgg for scalar k

nG = length(Wgg);
OUT = cell(1, nG);
for g = 1:nG
    OUT{g} = k .* Wgg{g};
end
end

