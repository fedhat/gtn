
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2016         %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function G_out = addGroup_crossed(G, d, k)
% == ai groups

r1 = floor(0.55 * d);
r2 = floor(0.75 * d);
c1 = floor(0.55 * k);
c2 = floor(0.75 * k);

G_new = { ...
    (1:r2)'     (1:c2)'; ...
    ((r2+1):d)' (1:k)'; ...
    (1:d)'      ((c2+1):k)' ; ...
    ((r1+1):d)' ((c1+1):k)' ...
    };

G_out = [G; G_new];
end

