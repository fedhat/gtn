%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = scalProd_EG(Wgg, Vgg) 

% standard scalar product on the space E_G
% Wgg and Vgg are supposed to belong to the same sapce, i.e. have the same
% groups

assert(iscell(Wgg));
assert(iscell(Vgg));

a = 0;
for g = 1:length(Wgg);
    a = a + Wgg{g}(:)' * Vgg{g}(:);
end

out = a;

end

