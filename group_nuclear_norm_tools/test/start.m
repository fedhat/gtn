
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2016         %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function start()

    experience_sparseRecovery();

end