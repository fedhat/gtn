%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2016         %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function L = fill_uniform(G,d,k)

L = zeros(d,k);
for g = 1:length(G)
    gx = length(G{g,1});
    gy = length(G{g,2});
    
    %            B = rand(1) * ones(gx,gy); %rank is 1
    B = (rand(1)/2 + 0.5) * ones(gx,gy);
    L = L + immersionInBigSpace(B, G, g, [d k]);
end
end
