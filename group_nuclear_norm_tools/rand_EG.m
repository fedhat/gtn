%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function OUT_EG = rand_EG(G) 
% generates an element in EG with  matrices filled uniform distributed
% entries in [0 1]

nG = size(G, 1);
OUT_EG = {};

for g = 1:nG
    OUT_EG{g} = rand(length(G{g, 1}), length(G{g, 2}));
end

end


