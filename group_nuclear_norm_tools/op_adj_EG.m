%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Wgg = op_adj_EG(W,G)  
% returns the adjoint of A, s.t  W=A(Wgg)
% A_adj(W)=(Pi_g(W) )_{g in G}

nG = size(G,1);
Wgg = cell(1, nG);
for g = 1:nG
    Wgg{g} = W(G{g,:});  %= projOnSmallSpace
end
end

