
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2016         %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function experience_sparseRecovery()

% This function launches FISTA with group Schatten norm regularization

% 1 generates groupsand low rank values on them
% 2 extracts some observations
% 3 recovers the initial matrix

clc
close all
clear all
addpath('..')
addpath('../utilities');

verbose = 0;

try
    rng(42)
catch
    if verbose > 3
        warning('random seed not set')
    end
end

set(0, 'DefaultLineLineWidth', 1)
set(0, 'DefaultAxesFontSize', 18)


%--------------------------------
% parameters for experience management

perc = 0.10; %percentage of observations
FOLDER_CUR = 'toy_figs';


if exist(FOLDER_CUR) ~= 7
    mkdir(FOLDER_CUR)
end

d = 410;
k = 500;



% maximum number of iterations
par.itMax = 50;
par.lip_0 = 10^-6; %10^-5 for size 500x500


par.tol = 0.01;


N = perc*d*k;


noise_sigma = [ 0.1 0.2 ];
sigma_name = {'0d1' '0d2'};

lambdas = [ 0.8  1 1.2 1.5 1.7 1.9 2.1  ] ./ N; %ok sigma=0.1
%lambdas = [ 0.8  1  1.5  3 5 10 50 ] ./ N;
%lambdas = [0.8 1 ]/N

DATA_SWITCH = 'smallRectangles_affine_a' ;

%--------------------------------

GRID = length(noise_sigma) * length(lambdas);


%-------------------------------------
%% full matrix generation
%-------------------------------------
switch DATA_SWITCH
    case 'full'
        G = {(1:d)' (1:k)' };
        % generation of full matrix
        L = rand(d,1) * rand(1,k);
        
    case 'smallRectangles_affine_a'
        
        % affine rectangles
        sa_G = add_small_groups({}, 5, d, k, floor(d/10), floor(k/8));
        
        sa_L =  fill_affine(sa_G, [d k]);
        
        a_G = addGroup_crossed({}, d, k); % {(1:d)' (1:k)'  };
        a_L = fill_affine(a_G, [d k]);
        
        L = sa_L + a_L;
        G = [ a_G ; sa_G];
        
        %normalization
        L = L - mean(L(:));
        L = L / std(L(:));
        
    case 'smallRectangles_flat'
        
        % small rectangles uniform
        sg_G = add_small_groups({}, 5, d, k, floor(d/10), floor(k/8));
        
        % add uniform value on each group
        sg_L = zeros(d,k);
        for g = 1:length(sg_G)
            gx = length(sg_G{g,1});
            gy = length(sg_G{g,2});
            
            %rank(B) is 1
            B = (rand(1) / 2 + 0.5) * ones(gx, gy);
            sg_L = sg_L + immersionInBigSpace(B, sg_G, g, [d k]);
        end
        
        a_G = {(1:d)' (1:k)'};
        u_L = sparse(d, k);
        
        L = sg_L + u_L;
        G = [ a_G ; sg_G];
        
        L = L - mean(L(:));
        L = L / std(L(:));
        
        
    otherwise
        error('misspelling')
end

% normalization
L = L - mean(L(:));
%   L = L/max(abs(L(:)));
L = L / std(L(:));

A_star = L;
A_star_clim = [min(A_star(:)), max(A_star(:))];
imagesc(A_star);
colorbar();
title('data')
saveas(gcf, [FOLDER_CUR '/data'], 'pdf')
saveas(gcf, [FOLDER_CUR '/data'], 'fig')

%-------------------------------------
% display blocks in LaTeX
%---------------------------------------
if verbose > 3
    for g = 1:size(G,1)
        imin = min(G{g,1});
        imax = max(G{g,1});
        jmin = min(G{g,2});
        jmax = max(G{g,2});
        % disp(['[ ' imin '... ' imax '],[' jmin '...' jmax ']'])
        fprintf('\\{%d ... %d\\}\\times\\{%d ...%d\\},\n', imin, imax, jmin, jmax)
    end
    
end

%-------------------------------------
%% train set generation
%-------------------------------------
% the observation set is independent from the groups
try
    idx = randperm(d * k, floor(d * k * perc));
catch
    if verbose > 3
        warning('code for old matlab')
    end
    idx = my_randperm(d * k, floor(d * k * perc));
end


MI = @(W) W(idx); %linear map mask  R^dk->R^n
MI_adj_ = @(x)  maskI_adj( x, idx, d, k); % R^n->R^dk

Aa = @(Wgg) assemble_EG(Wgg, G, [d k]);
Aa_adj = @(W) op_adj_EG(W, G);

errore = @(W) 1 / d / k * norm(W - A_star, 'fro')^2;
errore(zeros(d, k));

count = 1;
for j=1:length(noise_sigma)
    
    sigma = noise_sigma(j);
    
    % observed data
    X = A_star(idx) + sigma * randn(1, length(idx));
    
    close all
    figure
    imagesc(MI_adj_(X))
    title('observations')
    set(gca,'clim', A_star_clim);
    
    colorbar()
    
    saveas(gcf, [FOLDER_CUR '/data_noise_si_' sigma_name{j}], 'pdf')
    saveas(gcf, [FOLDER_CUR '/data_noise_si_' sigma_name{j}], 'fig')
    
    
    for l=1:length(lambdas)
        
        GRID_cur = (j-1)*length(lambdas) + l -1 ;
        
        par.lambda = lambdas(l);
          disp([' ' num2str(round(100 * GRID_cur / GRID)) '% done. Running: lambda=' num2str(par.lambda) ', sigma=' num2str(sigma)])
        
        % basic gradient, performance can be improved
        grad = @(Wgg) Aa_adj(MI_adj_( 1/N *( MI(Aa(Wgg)) - X)));
        f = @(Wgg) 0.5/N * norm( MI(Aa(Wgg)) - X ,'fro')^2;
        
        Wgg_0 = zeros_EG(G);
        
        par.G = G;
        
        out = fista_EG(Wgg_0, f, grad, par);
        
        Wgg_sol = out.sol.W;
        
        erisk_seq_cell{j,l} = [out.erisk_0 out.erisk];
        err(l,j) = errore(Aa(Wgg_sol));
        solutions{l,j} = Wgg_sol;
        
        try
            % collect metrics results
            fm = fields(out.metrics);
            for i=1:length(fm)
                metrics(count).lambda = par.lambda;
                metrics(count).sigma = sigma;
                metrics(count).error = cast(errore(Aa(out.sol.W)),'single');
                metrics(count).(fm{i}) = cast(out.metrics.(fm{i}),'single');
            end
        catch
            if verbose > 3
                warning('metrics not computed')
            end
        end
        
        
        count = count+1;
    end
end    
     disp([' 100% done.'])
     disp(['Results are in group_nuclear_norm_tools/tests/' FOLDER_CUR '/'])  


[lambda_best, l_argmin ] = min(err, [], 1); %suitable also when err has 1 row only

for j=1:size(err, 2)
    
    l = l_argmin(j);
    
    Wgg_sol =  solutions{l, j};
    
    close all
    figure
    imagesc(abs(Aa(Wgg_sol) - A_star))
    colormap(1 - gray)
    colorbar()
    title('error')
    saveas(gcf, [FOLDER_CUR '/error_si_' sigma_name{j} '_la_' num2str(l)], 'pdf')
    saveas(gcf, [FOLDER_CUR '/error_si_' sigma_name{j} '_la_' num2str(l)], 'fig')
    
    figure
    imagesc(Aa(Wgg_sol))
    colorbar()
    title('solution');
    set(gca,'clim', A_star_clim);
    
    saveas(gcf, [FOLDER_CUR '/solution_si_' sigma_name{j} '_la_' num2str(l)], 'pdf')
    saveas(gcf, [FOLDER_CUR '/solution_si_' sigma_name{j} '_la_' num2str(l)], 'fig')
end



if true
    %% plot solutions
    pt = {'x' 'o' '.'  '+'  's' 'd' 'v' 'v-' 'p' 'p-' 'h' 'h-'  };
    cl = {'r' 'g' 'b' 'c' 'm' 'k' };
    leg = {};
    figure
    hold on
    for j=1:length(noise_sigma)
        for l=1:length(lambdas)
            plot( erisk_seq_cell{j,l}, [ cl{j} pt{l} '-' ] );
            leg{end+1} = ['\sigma= ' num2str(noise_sigma(j)) ', \lambda= ' num2str(lambdas(l))];
            
        end
    end
    set(gca, 'yscale', 'log')
    ylabel('Empirical risk')
    xlabel('iterations')
    legend(leg, 'Location', 'eastoutside')
    
    saveas(gcf, [FOLDER_CUR '/last_vs_iter'], 'fig')
    saveas(gcf, [FOLDER_CUR '/last_vs_iter'], 'pdf')
    
end





%% print table
try
    tab = struct2table(metrics);
    disp(tab)
    writetable(tab, [FOLDER_CUR '/last_metrics.csv'], 'Delimiter',',');
    
catch
    if verbose > 3
        warning('table not printed')
    end
end


try
    ins.DATA_SWITCH = DATA_SWITCH;
    tab = struct2table(ins);
    disp(tab)
    writetable(tab, [FOLDER_CUR '/last_imputs.csv'], 'Delimiter',',');
    
catch
    if verbose > 3
        warning('table last_imputs.csv not printed')
    end
end


end

function W = maskI_adj(x, I, r, c)
% I: vector of indices
% x: entries to merge in W
% W: matrix

W = sparse(I, 1, x ,r * c, 1);
W = reshape(W, r, c);

end

function out = my_randperm(n, k) %same as randperm in new matlab versions

x = randperm(n);
out = x(1:k);
end



function L = fill_uniform(G, d, k)

L = zeros(d,k);
for g = 1:length(G)
    gx = length(G{g,1});
    gy = length(G{g,2});
    
    %rank is 1
    B = (rand(1)/2 + 0.5) * ones(gx, gy);
    L = L + immersionInBigSpace(B, G, g, [d k]);
end
end

function G_out = addGroup_crossed(G, d, k)
% == ai groups

r1 = floor(0.35 * d);
r2 = floor(0.65 * d);
c1 = floor(0.35 * k);
c2 = floor(0.65 * k);

G_new = { ...
    (1:r2)'     (1:c2)'; ...
    ((r2+1):d)' (1:k)'; ...
    (1:d)'      ((c2+1):k)' ; ...
    ((r1+1):d)' ((c1+1):k)' ...
    };

G_out = [G; G_new];
end


function  L =  fill_affine(G, taille);
% Takes each group end fills with affine function
% taille = [d k]
L = zeros(taille);
for g = 1:length(G);
    % size of the rectangle
    gx = length(G{g,1});
    gy = length(G{g,2});
    
    B = linspace(0, (2 * rand(1) -1), gx)'*ones(1, gy) + ...
        ones(gx, 1) * linspace(0, (rand(1) -1), gy);
    L = L + immersionInBigSpace( B, G, g, taille);
end

end
