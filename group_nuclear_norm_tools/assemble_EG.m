%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function   W = assemble_EG(Wgg, G, dims)
% dims: dimensions of the space where W lives
%computes  W=A(Wgg)=sum i_g(W_g)
% Wgg{g} is W_g

assert(is_EG(Wgg, G));
nG = length(Wgg);

W = zeros(dims);
for g = 1:nG
    T = zeros(dims);
    T(G{g,:})= Wgg{g}; % immersion
    W = W + T;
end

end
