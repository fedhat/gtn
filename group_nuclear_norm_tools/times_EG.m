%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function OUT = times_EG(Wgg, Vgg) 
% overloads the elementwise product .*
% Returns: Wgg .* Vgg 

assert( iscell(Wgg) & iscell(Vgg))
OUT = cellfun(@times,  Wgg, Vgg, 'UniformOutput', false);
     
end

