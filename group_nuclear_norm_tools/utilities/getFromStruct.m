%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = getFromStruct(par, parField, defaultVal)
% par: structure
% parField: name of the field we try to get
% defaultVal: value to set in case there is no such field

% example
% to take l = param.lambda and set as default =10 if it is not defined
%l = getFromStruct(param,'lambda',10)

assert(ischar(parField));

if isfield(par, parField)
    out = par.(parField);
else
    out = defaultVal;
end
end
