%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function OUT = immersionInBigSpace(W_g, G, i, dims)
%example  G={[1 3] [1 3 4]; [1 2 3] [2 3]}

OUT = zeros(dims);
OUT(G{i,:}) =  W_g;

end