%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = my_randperm(n,k) 
%same as randperm in new matlab versions, used for octave compatibility

x = randperm(n);
out = x(1:k);
end
