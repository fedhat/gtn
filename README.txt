

LICENSE
The package group_nuclear_norm_tools is distributed under CeCILL free software license.TUTORIAL
To run the toy example run the file group_nuclear_norm_tools/test/start.m with octave or Matlab.CITATION
If you use GroupTraceNormTools, please cite Chapter 2 of the following Ph.D. thesis:
Nonsmooth optimization for statistical learning with structured matrix regularization, F. Pierucci 2016
@phdthesis{pierucci:tel-01572186,  TITLE = {{Nonsmooth Optimization for Statistical Learning with Structured Matrix Regularization}},  AUTHOR = {Pierucci, Federico},  URL = {https://hal.inria.fr/tel-01572186},  SCHOOL = {{Universit{\'e} Grenoble-Alpes}},  YEAR = {2017},  MONTH = Jun,  KEYWORDS = {first-order optimization ; conditional gradient ; smoothing ; nuclear-norm ; machine learning ; mathematical optimization ; methodes de premier ordre ; gradient conditionnel ; lissage ; norme nucleaire ; apprentissage automatique ; optimisation mathematique},  TYPE = {Theses},  PDF = {https://hal.inria.fr/tel-01572186/file/main_manuscript.pdf},  HAL_ID = {tel-01572186},}