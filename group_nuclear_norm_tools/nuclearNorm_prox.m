%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = nuclearNorm_prox(W, k) 
% returns: argmin_Y ||Y||_tr + 1/(2k) |Y-W|_2^2
% this is the prox related to the nuclear norm 

[U, s, V] = svd(full(W), 'econ');
d = soft_thresholding(diag(s), k);
out = U*diag(d)*V';

end