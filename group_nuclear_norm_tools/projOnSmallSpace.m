%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function OUT = projOnSmallSpace(W, G, i)
%example  G={[1 3] [1 3 4]; [1 2 3] [2 3]}
% W = rand(5);
%  projOnSmallSpace(W,G,1)

% G contains indeces: G={row col; row col; row col}
%                       {    g1 ;    g2  ;   g3   }
% where each pair "row col" is relates to the same group


if nargin < 3
    i = 1;
end

assert(length(i) == 1);

OUT = W(G{i,:});

end
