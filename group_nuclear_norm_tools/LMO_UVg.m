%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2015-2016    %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [u_g_hat, v_g_hat, g_hat] = LMO_UVg(x, G, par)
% linear minimization oracle for the group schatten norm, p=1, alpha=1,1,1,

% Ugg: sequence of matrices of size d_g*r_g , for all g in G
% Vgg: sequence of matrices of size k_g*r_g , for all g in G

% g_hat: integer index of selected bloch
% u_g_hat: 1 vector of length d_(g_hat)
% v_g_hat: 1 vector of length k_(g_hat)

if nargin==0
    clc
    close all
    addpath('utilities')
    
    d = 50;
    k = 20;
    
    %%  groups
    
    r1 = floor(0.55 * d);
    r2 = floor(0.75 * d);
    c1 = floor(0.55 * k);
    c2 = floor(0.75 * k);
    
    G = { ...
        (1:r2)'     (1:c2)'; ...
        ((r2+1):d)' (1:k)'; ...
        (1:d)'      ((c2+1):k)' ; ...
        ((r1+1):d)' ((c1+1):k)' ...
        };
    
    nG = size(G,1);
    
    
    par.space = 'UVgg'
    
    %%generate Ugg , Vgg
    r = 3; % rank
    for g = 1:nG
        U = randn(length(G{g,1}),r);
        U = U/norm(U,'fro');
        Ugg{g} = U;
        V = randn(length(G{g,2}),r);
        V = V/norm(V,'fro');
        Vgg{g} = V;
    end
    
    [u, v, g] = LMO_UVg({Ugg,  Vgg}, G, par)
    
    par.space = 'Wgg'
    
    Wgg = randn_Eg(G);
    
    [u, v, g] = LMO_UVg(Wgg, G, par)
    
    
    % 'g';
    
    
else
    
    SPACE = par.space;
    switch SPACE
        case 'Wgg'
            
            Wgg = x;
            nG = size(G,1);
            
            z_hat = -Inf;
            for g = 1:nG
                Wg = Wgg{g};
                [u, tmp, v] = svds( Wg, 1);
                z_g = u' * Wg * v;
                if z_hat < abs(z_g)
                    z_hat = z_g;
                    u_g_hat = u; %changes dimension depending on g
                    v_g_hat = v; %changes dimension depending on g
                    g_hat= g;
                end
            end
            
        case 'UVgg' % probably unuseful, as the gradient is a matrix sequence Wgg
            
            Ugg = x{1};
            Vgg = x{2};
            
            %  assert(is_EG(Wgg, G));
            nG = length(Ugg);
            %assert
            for g=1:nG
                %same rank
                assert(size(Ugg{g},2)== size(Vgg{g},2) )
                
                % same size of block
                assert(size(Ugg{g},1)==length(G{g,1}))
                assert(size(Vgg{g},1)==length(G{g,2}))
            end
            
            z_hat = -Inf;
            for g = 1:nG
                Ug = Ugg{g};
                Vg = Vgg{g};
                %  Wg==Ug*Vg'
                
                [u, tmp, v] = svds( Ug * Vg', 1);
                z_g = (u' * Ug) * (Vg' * v); %u'*Wg*v
                if z_hat < abs(z_g)
                    z_hat = z_g;
                    u_g_hat = u; %changes dimension depending on g
                    v_g_hat = v; %changes dimension depending on g
                    g_hat= g;
                end
            end
            
        otherwise
            error('misspelling')
            
    end
end