%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%  Coded by Federico Pierucci at Inria, 2016         %
%                                                    %
%  This file is part of software distributed under   %
%  the CeCILL free software license.                 %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function  L = fill_affine(G, taille)
% Takes each group end fills with affine function
% taille = [d k]
L = zeros(taille);
for g = 1:length(G);
    % size of the rectangle
    gx = length(G{g,1});
    gy = length(G{g,2});
    
    
    
    B = linspace(0,(2*rand(1)-1),gx)'*ones(1,gy) + ...
        ones(gx,1) * linspace(0,(rand(1)-1),gy) ;
    L = L + immersionInBigSpace( B, G, g, taille);
end

end
