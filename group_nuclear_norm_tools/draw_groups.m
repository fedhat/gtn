function draw_groups(G, par) % pierucci 19-1-2016

if nargin == 0
    
    close all
    % matrix size
    d = 500;
    k = 500;
    
    % 4 disjoint groups
    rr = floor(0.65 * d);
    cc = floor(0.75 * k);
    
    G = {...
        (1:rr)'     (1:cc)'  ;...
        ((rr+1):d)' (1:cc)'  ;...
        (1:rr)' ((cc+1):k)'  ;...
        ((rr+1):d)' ((cc+1):k)' ...
        };
    
    
    
    x = randn(500,500);
    imagesc(x);
    colorbar()
    colormap(gray)
    
    
    draw_groups(G,[])
    

    
else    
    
    nG = size(G,1);
%    c = {'c' 'm' 'y'}
    
    for g = 1:nG
        
        
        y0 =  min(G{g,1});
        yb =  max(G{g,1});
        x0 =  min(G{g,2});
        xr =  max(G{g,2});
        
        h = yb-y0;
        w = xr-x0;
        
        a = 2;
       
        rectangle('Position',[x0+a y0+a w-2*a h-2*a] ,...
            'LineWidth',2 ,   ...
            ...%    'EdgeColor', c{randi(3)}  )
            'EdgeColor', 'y'  )
        
    end
    
end
end